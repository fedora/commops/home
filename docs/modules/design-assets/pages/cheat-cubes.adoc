include::ROOT:partial$attributes.adoc[]

= Cheat cubes

Fedora Cheat Cubes are a long time community favorite. They are designed to print, cut, and tape together cubes covered with various Fedora related designs. The traditional theme for Cheat Cubes is Packaging- but we modernized this project by creating other variations!

== Packaging 

Find the Packaging Cheat Cube source files https://pagure.io/fedora-commops/blob/main/f/docs/modules/all-assets/Cheat%20Cubes[here].

image::cheatcube_packaging.png[S]

== How to Join Fedora

Find the How to Join Fedora Cheat Cube source files https://pagure.io/fedora-commops/blob/main/f/docs/modules/all-assets/Cheat%20Cubes[here].

image::cheatcube_howtojoinFedora_version2.png[Static]

== Four Foundations

Find the Four Foundations Cheat Cube source files https://pagure.io/fedora-commops/blob/main/f/docs/modules/all-assets/Cheat%20Cubes[here].

image::cheatcube_fourfoundations.png[]

== Community Outreach Teams

Find the Community Outreach Teams Cheat Cube source files https://pagure.io/fedora-commops/blob/main/f/docs/modules/all-assets/Cheat%20Cubes[here].

image::cheatcube_communityoutreachteams.png[]
