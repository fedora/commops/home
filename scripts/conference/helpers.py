import logging

def setup_logging(verbosity, base_level=logging.WARNING):
    # https://xahteiwi.eu/resources/hints-and-kinks/python-cli-logging-options/
    verbosity = min(verbosity, 2)
    loglevel = base_level - (verbosity * 10)
    logging.basicConfig(level=loglevel,
                        format='%(message)s')


def remap_dict(keymap:dict, data:dict, include_all=False):
    """remap the keys from one dict to different keys

    Args:
        keymap (dict): the mapping to apply to the dict keys
        data (dict): a single dict to perform the remapping on
        include_all (bool, optional): Whether to keep items that are not explicitly remapped. Defaults to False.
    """
    remapped_dict = []
    # https://stackoverflow.com/a/644314
    for name, val in data.items():

        # if there is a remapping, take it
        if name in keymap:
            name = keymap.get(name)
        # if no remapping, and skipping unmapped items,
        # skip to the next iteration
        elif not include_all:
            continue 
            
        # otherwise the existing name is fine
        remapped_dict.append((name, val))

    return dict( remapped_dict )


def remap_dict_list(talks, mapping={}, include_all=False):
    for t in talks:
        yield remap_dict(mapping, t, include_all=include_all)
