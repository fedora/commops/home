import argparse
import requests
from urllib.parse import urlparse
from pathlib import Path
import json
import csv
import logging
import sys
from helpers import setup_logging, remap_dict, remap_dict_list

logger = logging.getLogger(__name__)

def non_empty_str(item):
    """returns False if the input is None or an empty string
    """
    if item is None or item == "":
        return False
    return True

def human_room_identifier_from_name(room_name:str):
    """Convert a longer human readable room name into a shorter, still mostly human readable, identifier

    Args:
        room_name (str): the name of the room to convert
    """
    room_name = str(room_name).replace(" ", "-")
    return ''.join(e for e in room_name if e.isalnum())

def build_csv_filename(event_id, day_number, room_name):
    return f"{event_id}_day-{day_number}_{human_room_identifier_from_name(room_name)}.csv"


def combine_person_names(names: list[str]):
    if len(names) == 0:
        return ""
    elif len(names) == 1:
        return names[0]
    elif len(names) > 1:
        last = names[-1]
        rest = names[:-1]

        return ", ".join(rest) + ", and " + last

def transform_talk(talk_data:dict) -> dict:
    """Transform the data about a talk so it can more easily be used and saved for later stages of the pipeline

    Args:
        talk_data (dict): the talk data to be extracted

    Returns:
        (dict): a dict of the talk data, transformed and ready to be remapped to the CSV schema
    """

    # fundamentally this just passes the data through with minor transformation and flattening
    talk_data = dict(talk_data)


    speakers = [ p.get("public_name") for p in talk_data.get("persons")]

    talk_data["speakers"] = combine_person_names(speakers)

    talk_data["do_not_record"] = str(talk_data["do_not_record"])

    return talk_data

def transform_step(talks):
    for t in talks:
        yield transform_talk(t)

def remap_step(talks, mapping={}, include_all=False):
    for t in talks:
        yield remap_dict(mapping, t, include_all=include_all)


def validate_pretalx_url(url:str):
    """validate a pretalx url and extract the useful components of it.

    Examples of valid pretalx urls:

    https://pretalx.com/devconf-us-2024/
    http://pretalx.com/devconf-us-2024/
    https://cfp.fedoraproject.org/flock-2024/
    https://cfp.fedoraproject.org/flock-2024/schedule
    https://cfp.fedoraproject.org/flock-2024/schedule/

    Args:
        url (str): the pretalx url to validate

    Raises:
        ValueError: This exception is raised if the input URL value is incorrect in some way

    Returns:
        tuple(str, str): a tuple containing the pretalx event ID and domain name
    """
    # TODO: this may fail if the HTTPS is omitted. need to check for this
    # per the urlparse docs:
        # Following the syntax specifications in RFC 1808, urlparse recognizes a netloc only if it is properly introduced by ‘//’. Otherwise the input is presumed to be a relative URL and thus to start with a path component.
    
    if not url.startswith("http"):
        raise ValueError("The URL provided must start with http:// or https://")
    
    pretalx_event_link = urlparse(url)
    pretalx_path = pretalx_event_link.path

    # validate the path contains an event identifier
    if pretalx_path == "":
        raise ValueError("The URL provided must contain at least one path component (i.e. https://example.com/path)")

    path_segments = pretalx_path.split("/")
    path_segments = list(filter(non_empty_str, path_segments))

    # validate that the url ends in "schedule"
    # path_segments[1] == "schedule"

    pretalx_domain = pretalx_event_link.netloc
    pretalx_event_id = path_segments[0]
    return (pretalx_domain, pretalx_event_id)


def grab_schedule_data(pretalx_event_url, cachepath="schedule.json", use_cache=True, dry_run=False):
    # request the schedule data if it isnt already cached

    # TODO: detect and update version of schedule
    cache_path = Path(cachepath)
    if not cache_path.exists() or use_cache == False:
        response = requests.get(pretalx_event_url + "export/schedule.json")
        scheduleData = response.json()
        if not dry_run:
            cache_path.write_text(json.dumps(scheduleData))
            logger.info(f"not writing {cache_path} due to dry run mode")
    else:
        scheduleData = json.loads(cache_path.read_text())
    
    return scheduleData.get("schedule")


def generate_CSV_from_pretalx(scheduleData, pretalx_event_id, dry_run=False, outputDir=Path("output"), force=False):
    # extract each room of the schedule data
    conference_days = scheduleData.get("conference").get("days")
    if not outputDir.exists():# and not dry_run
        outputDir.mkdir()

    for day in conference_days:
        for room, talks in day.get("rooms").items():
            logger.debug(f"{day.get('date')} - {room} Talks:")

            room_folder = ensure_room_folder(pretalx_event_id, day.get('index'), room, parent_dir=outputDir)

            output_csv = room_folder / build_csv_filename(pretalx_event_id, day.get("index"), room)

            datakey_mapping = {
                "title": "TalkName",
                "speakers": "Speaker",
                "slug": "Identifier",
                "track": "TrackName",
                # these are less important
                "subtitle": "TalkSubtitle",
                "do_not_record": "DoNotRecord",
                "url": "TalkURL",
                # abstract
                # links
                # attachments

            }
            #  use a generator chain to process the talks one by one, first transforming the data into a flatter structure, and then remapping the keys
            processed_talks = remap_step(transform_step(talks), mapping=datakey_mapping)

            processed_talks = list(processed_talks)
            logger.debug(processed_talks)
            
            # export to CSV (maybe define a python schema for this data?)
            destination = output_csv

            if not dry_run:
                if destination.exists() and not force:
                    logger.info(f"not writing {destination} as it would overwrite an existing file. Specify --force to write anyway")
                else:
                    with destination.open('w') as csvfile:
                        writer = csv.DictWriter(csvfile, fieldnames=datakey_mapping.values())

                        writer.writeheader()
                        writer.writerows(processed_talks)
            else:
                logger.info(f"not writing {destination} due to dry run mode")
            
            logger.debug("")

def create_description_replacement_tag(label):
    return f"##{label.strip().upper()}##"

def ensure_room_folder(pretalx_event_id:str, day:int, room:str, parent_dir=Path(".")):
    room_folder = parent_dir / f"{pretalx_event_id}_day-{day}_{human_room_identifier_from_name(room)}"
    if not room_folder.exists():# and not dry_run
        room_folder.mkdir()
    
    return room_folder

def generate_descriptions_from_pretalx(scheduleData, pretalx_event_id, description_template, outputDir=Path("output"), dry_run=False, force=False):
    # extract each room of the schedule data
    conference_days = scheduleData.get("conference").get("days")
    if not outputDir.exists():# and not dry_run
        outputDir.mkdir()


    for day in conference_days:
        for room, talks in day.get("rooms").items():
            logger.debug(f"{day.get('date')} - {room} Talks:")
            # make a folder
            room_folder = ensure_room_folder(pretalx_event_id, day.get('index'), room, parent_dir=outputDir)

            # process speaker names into a single string instead of a nested dict
            talks = transform_step(talks)


            # datakey_mapping = {
            #     "title": "TalkName",
            #     "speakers": "Speaker",
            #     "slug": "Identifier",
            #     "track": "TrackName",
            #     # these are less important
            #     "subtitle": "TalkSubtitle",
            #     "do_not_record": "DoNotRecord",
            #     "url": "TalkURL",
            #     # abstract
            #     # links
            #     # attachments

            # }
    
            for talk in talks:
                logger.debug(f"\t {talk.get('title')}")

                description_filename = f"{talk.get('slug')}.txt"

                description = str(description_template)

                description = description.replace(
                    create_description_replacement_tag("abstract"),
                    talk.get("abstract")
                )
                description = description.replace(
                    create_description_replacement_tag("speakers"),
                    talk.get("speakers")
                )
                description = description.replace(
                    create_description_replacement_tag("pretalx_link"),
                    talk.get("url")
                )

                description_folder = room_folder / "descriptions"
                if not description_folder.exists():
                    description_folder.mkdir()
           
                # export to the text file (maybe define a python schema for this data?)
                destination = (description_folder / description_filename)

                if not dry_run:
                    if destination.exists() and not force:
                        logger.info(f"not writing {destination} as it would overwrite an existing file. Specify --force to write anyway")
                    else:
                        destination.write_text(description)
                else:
                    logger.info(f"not writing {destination} due to dry run mode")
            
            logger.debug("")




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process the schedule data from PreTalx into CSV files for the Fedora virtual event pipeline')
    parser.add_argument('eventlink', help='the url of the pretalx event to grab the schedule for')
    # parser.add_argument('--talks', help='the path to the CSV file containing the ordered data for the talk segments (in this video only)')
    parser.add_argument('-n', '--dry-run', action='store_true', help='do not write any files to disk, but perform other actions')
    parser.add_argument('-v', '--verbose', action='count', dest='verbosity', default=0, help="verbose output (repeat for increased verbosity)")
    parser.add_argument('-q', '--quiet', action='store_const', const=-1, default=0, dest='verbosity', help="quiet output (show errors only)")
    parser.add_argument('-f', '--force', action='store_true', help='override protections against overwriting files')
    # TODO: add titlekey arg to specify what key to use for that field?
    # parser.add_argument('--titlekey', type=str, default="TalkName", help='The name of the heading used to look up the title of each talk. Defaults to "TalkName"')

    args = parser.parse_args()

    setup_logging(args.verbosity)

    # validate and parse out the various parts of the url provided
    try:
        pretalx_domain, pretalx_event_id = validate_pretalx_url(args.eventlink)
    except ValueError as e:
        logger.error(e)
        sys.exit(1)

    pretalx_event_url = f"https://{pretalx_domain}/{pretalx_event_id}/schedule/"

    scheduleData = grab_schedule_data(pretalx_event_url, dry_run=args.dry_run)

    # validate API version
    # if scheduleData.get("version") != "_4":
    #     raise ValueError("Version code in Pretalx API response is not expected. Pretalx probably changed something - verify the script still works")

    generate_CSV_from_pretalx(scheduleData, pretalx_event_id, dry_run=args.dry_run, force=args.force)

    description_template_file = Path("description.template.txt")
   
    if description_template_file.exists():
        description_template = description_template_file.read_text()

        generate_descriptions_from_pretalx(scheduleData, pretalx_event_id, description_template, dry_run=args.dry_run, force=args.force)
