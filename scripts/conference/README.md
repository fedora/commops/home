# Conference
This is more of a catch-all script for downloading and reformatting various metadata from Pretalx into formats that can be usable by other parts of the [livestream processing SOP](https://docs.fedoraproject.org/en-US/marketing/sops/stream-uploads/).

Currently it:
- Creates the CSV files that the other automatins in the pipeline use as input
- Creates text files containing filled-in descriptions for each talk to make reupload to YouTube easier
- Writes all output into a directory structure organized per-room, which can optionally serve as a working space to use for future scripts


## You will need
- the link to the event on pretalx (it can be any link to a pretalx page as long as it contains the event ID. Valid examples include: https://pretalx.com/[event-id]/schedule, https://pretalx.com/[event-id], https://custom-domain-pretalx-instance.org/[event-id]/talk/[talk-id]/. [Pages with pretalx embeds](https://www.devconf.info/us/schedule/) will NOT work. if it doesnt say "powered by pretalx" at the bottom, it likely wont work)
- Reasonably modern Python (tested and developed using Python 3.12)
- an environment with the dependencies installed ( `pip install -r requirements.txt`)

## Usage

### Dependencies
- requests (python package)

To install dependencies, run `pip install -r requirements.txt` in a relatively modern python environment

### Running

1. If you would like to generate youtube descriptions, copy or rename `description.template.txt.sample` to `description.template.txt` and edit it to include your youtube description template.
2. Run the script: `python3 ./conference.py <pretalx schedule URL>`. The output will be located in the `output` directory
3. If you choose to use this generated directory structure for the remainder of your video processing, it is recommended to copy the contents of the `output` directory somewhere else on your system to avoid accidentally regenerating it
