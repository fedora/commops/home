# from python_sponsorblock.exceptions import 
import sponsorblock as sb
from sponsorblock.models import Segment
from datetime import timedelta
import argparse
import csv
from dataclasses import dataclass, field
from helpers import setup_logging, remap_dict_list
import logging
import sys

logger = logging.getLogger(__name__)

@dataclass
class VideoChunk:
    start: float
    end: float
    is_intermission: bool
    title: str = field(default="")
    identifier: str = field(default="", hash=False)


    @classmethod
    def from_neighbours(cls, before, title, after, total_video_duration:float=None, identifier:str=""):
        if total_video_duration is None and after is None:
            logger.warning("A fallback value for total_video_duration is needed because after is None when creating a segment from neighbours. Using a default value as a fallback")
            total_video_duration = 9999999999.99
        before = 0 if before is None else before.end
        after = total_video_duration if after is None else after.start
        return cls(before, after, is_intermission=False, title=title, identifier=identifier)

    @property
    def title_or_intermission(self):
        return "Intermission" if self.is_intermission else self.title

    @property
    def duration(self):
        return self.end - self.start

    def __repr__(self):
        return f"VideoChunk(start={timedelta(seconds=self.start)}, end={timedelta(seconds=self.end)}, is_intermission={self.is_intermission}, title='{self.title}')"

    def youtube_format(self):
        start_time = round_timedelta_for_yt(timedelta(seconds=self.start))
        return f"{start_time} {self.title_or_intermission}"

    def full_format(self):
        start_time = timedelta(seconds=self.start)
        end_time = timedelta(seconds=self.end)
        return f"{start_time}-{end_time} {self.title_or_intermission}"

def round_timedelta_for_yt(delta):
    return timedelta(seconds=round(delta.total_seconds()))

def get_intermission_chunks(video_url, sb_client = None):
    sb_client = sb_client or sb.Client()


    # TODO: this probably wont handle having multiple submissions that overlap/conflict, maybe we should sanity check that
    segments = sb_client.get_skip_segments(video_url)
    total_video_duration = sb_client.get_segment_info(segments[0])[0].video_duration
    logger.debug(f"video duration {total_video_duration}")
    logger.debug(f"fetched {len(segments)} segments from sponsorblock")

    intermission_segments = list(filter(lambda s: s.category == "intro", segments))

    intermission_segments = [VideoChunk(s.start, s.end, True, "") for s in intermission_segments]

    return intermission_segments, total_video_duration


def get_talk_chunks(talks_csv_path, titlekey="Title"):
    talks_data = None

    if talks_csv_path:
        logger.debug(f"reading talks data")

        with open(talks_csv_path) as csvfile:
            reader = csv.DictReader(csvfile)
            talks_data = list(reader)

        talks_data = list(remap_dict_list(talks_data, {titlekey: 'talktitle'}, include_all=True))

    else:
        logger.debug(f"generating talk names because no talk data was specified")
        talks_data = [{'talktitle': f"Talk #{i}"} for i in range(len(segments))]
    
    return talks_data

def talk_title_to_identifier(talkname:str):
    words = talkname.split(" ")
    words = [to_alnum(w).strip().lower() for w in words]
    
    return '_'.join(words)

def to_alnum(word:str):
    return ''.join(e for e in word if e.isalnum())

def get_talk_identifier(talk, default=""):
    identifier = talk.get("identifier", default)
    if identifier == "":
        identifier = talk.get("Identifier", default)
    
    return identifier


def combine_intermission_and_content_chunks(intermission_segments:list[VideoChunk], talks_data:list[VideoChunk], total_video_duration=None):
    
    logger.debug(f"Identified {len(intermission_segments)} segments of intermission type:")

    for s in intermission_segments:
        logger.debug(f"\t{s}")

    logger.debug(f"talks data contains {len(talks_data)} talks")


    begins_with_intermission = intermission_segments[0].start == 0

    logger.debug(f"provided video does {'not ' if not begins_with_intermission else ''}start with an intermission")

    video_chunks = []

    # minimum gap to consider displaying the intermission (some are <1 second intermissions just to insert a simple cut)
    minimum_intermission_length = 1

    # a number representing the parity between talk and intermission data
    # it is the difference between how many talks there are and how many intermissions
    # -1 means theres one less intermission than talks
    # 0 means a 1:1 correlation between intermissions and talks
    # 1 means theres one more intermission than talks
    talk_intermission_parity = len(intermission_segments) - len(talks_data)

    if abs(talk_intermission_parity) >= 2:
        logger.error(f"Talk-Intermission Mismatch. The number of talks provided ({len(talks_data)}) isnt close enough to the number of intermissions detected ({len(intermission_segments)}). Maximum acceptable difference is 1")

    # Determine whether the end of the video occurs during an intermission
    # based on the values we already know (i.e. make it make sense given the inputs)
    # bwi = begins_with_intermission
    # ewi = ends_with_intermission
    # | Parity  |   bwi   |  !bwi   |
    # | ------- | ------- | ------- |
    # |   -1    |  ERROR  |  ewi=F  | Error = Theres not enough intermissions
    # |    0    |  ewi=F  |  ewi=T  |
    # |    1    |  ewi=T  |  ERROR  | Error = Theres too many intermissions

    if talk_intermission_parity == -1 and begins_with_intermission:
        logger.error(f"Talk-Intermission Matching Impossible." 
            f"The number of intermissions detected ({len(intermission_segments)}) is too low to"
            f"be interspersed between the number of talks ({len(talks_data)})")
        sys.exit(1)

    if talk_intermission_parity == 1 and not begins_with_intermission:
        logger.error(f"Talk-Intermission Matching Impossible." 
            f"The number of intermissions detected ({len(intermission_segments)}) is too high to"
            f"be interspersed between the number of talks ({len(talks_data)})")
        sys.exit(1)

    # default case: parity == 0
    has_ending_intermission = not begins_with_intermission

    # if the parity is not zero (remember the errors were dealt with already),
    # then adjust the values of EWI to make sense
    if abs(talk_intermission_parity) == 1:
        has_ending_intermission = not has_ending_intermission

    for i,talk in enumerate(talks_data):

        logger.info(f"Processing talk {i}: ")
        logger.debug(f"\t{talk}")

        is_first_talk = talk == talks_data[0]
        is_last_talk = talk == talks_data[-1]

        if is_first_talk:
            logger.debug(f"\tThis is the first talk")

        if is_last_talk:
            logger.debug(f"\tThis is the last talk")

        intermission_before = None
        if not is_first_talk:
            intermission_before = intermission_segments[i-1] if not begins_with_intermission else intermission_segments[i]
        elif is_first_talk and begins_with_intermission:
            intermission_before = intermission_segments[0]
    
        logger.debug(f"\tIntermission before index {intermission_segments.index(intermission_before)}")

        intermission_after = None
        if not is_last_talk:        
            # TODO: this makes some assumptions that the ending (whether its an intermission or not)
            # has anything to do with whether the beginning was an intermission
            # this should maybe use has_ending_intermission at some point in the future
            intermission_after = intermission_segments[i] if not begins_with_intermission else intermission_segments[i+1]
        
            logger.debug(f"\tIntermission after index {intermission_segments.index(intermission_after) or 'none'}")
        # intermission case
        # intermission_before will be none if is_first_talk = True and and begins_with_intermission = False
        if intermission_before is not None:
            if intermission_before.duration > minimum_intermission_length:
                video_chunks.append(intermission_before)
            else:
                logger.debug("Skipping intermission before chunk due to duration lower than threshold")

        # talk case
        identifier = get_talk_identifier(talk)

        if identifier == "":
            identifier = talk_title_to_identifier(talk['talktitle'])

        video_chunks.append(
            VideoChunk.from_neighbours(
                intermission_before,
                talk['talktitle'],
                intermission_after,
                total_video_duration=total_video_duration,
                identifier=identifier
            )
        )

    return video_chunks


def print_segments(video_chunks, full_format=False,):

    for chunk in video_chunks:

        if full_format:
            print(chunk.full_format())
        else:
            print(chunk.youtube_format())

def write_to_losslesscut_file(video_chunks, include_intermissions=False):

    if not include_intermissions:
        video_chunks = list(filter(lambda c: not c.is_intermission, video_chunks))

    with open('losslesscut.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows([(c.start, c.end, c.identifier) for c in video_chunks])
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process the timestamps from Sponsorblock')
    parser.add_argument('video', help='the url of the video to generate timestamps for')
    parser.add_argument('--talks', help='the path to the CSV file containing the ordered data for the talk segments (in this video only)')
    parser.add_argument('--full', action='store_true', help='output the timestamps in full-detail format')
    parser.add_argument('--titlekey', type=str, default="TalkName", help='The name of the heading used to look up the title of each talk. Defaults to "TalkName"')
    parser.add_argument('-v', '--verbose', action='count', dest='verbosity', default=0, help="verbose output (repeat for increased verbosity)")
    parser.add_argument('-q', '--quiet', action='store_const', const=-1, default=0, dest='verbosity', help="quiet output (show errors only)")

    args = parser.parse_args()

    setup_logging(args.verbosity)

    intermissions, total_video_duration = get_intermission_chunks(args.video)
    talks = get_talk_chunks(args.talks, titlekey=args.titlekey)

    segments = combine_intermission_and_content_chunks(intermissions, talks, total_video_duration=total_video_duration)

    print_segments(segments, full_format=args.full)


    identifiers = [c.identifier for c in segments]
    if all(lambda c: c.is_intermission or c.identifier != "", segments) and len(set(identifiers)) == len(identifiers):
        write_to_losslesscut_file(segments)
    else:
        logger.warning("Not printing losslesscut file because not every content line had a unique identifier value specified")
