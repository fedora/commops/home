# Timestamps

This is a simple python script to take the timestamps from sponsorblock per the [livestream processing SOP](https://docs.fedoraproject.org/en-US/marketing/sops/stream-uploads/) and automatically reformat them for use in YouTube, losslesscut, and other parts of the pipeline.

This script will print the timestamps to the console in a YouTube-compatible format, labeling the talks with talk names from the CSV (if provided). This output can be pasted into the YouTube video description and will cause YouTube to automatically create chapter markers for viewers to be able to skip to that moment in the content

If the CSV contains an identifier value for the talk, the script will also output a `losslesscut.csv` file that can be dragged into the LosslessCut video editor to make the editing process way easier in later stages of the pipeline.


## Input Requirements
The script at a minimum requires a YouTube url to a sponsorblock-timestamped video to be provided.

The script should also be run in a reasonably modern Python environment (developed with Python 3.12) with the dependencies installed ( `pip install -r requirements.txt`).

### CSV file
An optional CSV file containing the names and an identifier string for each talk in the order they appear in the YouTube video can also be supplied to enable a lot more useful shortcuts. If your CSV was generated from a pretalx schedule by the [conference](../conference/) script, these requirements should already be satisfied.

If this CSV is not present, sequential talk names will instead be generated. Automated editing of clips will also be impacted (see below)

The parsing for the CSV assumes the column for the talk title is named `TalkName` (exact case), however it can be overridden with the `--titlekey` command line argument. The column for the identifier value should be called either `identifier` or `Identifier`. 


## Usage
The basic way to use the script looks something like this:
`python3 ./timestamps.py YOUTUBE_URL --talks /path/to/talks.csv`

To learn more about other available options, run `python3 ./timestamps.py --help`. This will display some helptext with a list of possible arguments that the script accepts.

## Example

Here is an example using the day 1 livestream from the F40 release party as an example 
`python3 ./timestamps.py https://www.youtube.com/watch?v=NxMzWuHhyAY --talks ./release-party-40-day1.csv`

The csv content was as follows (most of this information is extraneous):
```csv
TalkName,PresenterNames,FileName
Opening Remarks,"Matthew Miller, Aoife Moloney, Justin W. Flory",Opening Remarks
Passim Peer to Peer Metadata,Richard Hughs,Passim Peer to Peer Metadata
Mentored Projects Update,Fernando Mancera,Mentored Projects Update
Fedora COSMIC SIG,Ryan Brue,Fedora COSMIC SIG
Git Forge ARC Investigation Update,"Tomas Hckra, Akashdeep Dhar",Git Forge ARC Investigation Update
Fedora Week of Diversity 2024,"Emma Kidney, Chris Idoko",Fedora Week of Diversity 2024
EPEL 10 Overview,Carl George,EPEL 10 Overview
KDE Plasma 6,Neal Gompa,KDE Plasma 6
Ultramarine Linux,Owen Zimmerman,Ultramarine Linux
Fedora Asahi Linux Remix,"Davide Cavalca, Neal Gompa",Fedora Asahi Linux Remix
Closing Remarks,"Aoife Moloney, Justin W. Flory",Closing Remarks
```

Output:
```
0:00:00 Opening Remarks
0:31:59 Intermission
0:32:14 Passim Peer to Peer Metadata
0:47:47 Intermission
0:47:55 Mentored Projects Update
1:00:36 Intermission
1:01:04 Fedora COSMIC SIG
1:33:46 Intermission
1:42:33 Git Forge ARC Investigation Update
1:59:53 Intermission
1:59:59 Fedora Week of Diversity 2024
2:17:19 Intermission
2:57:32 EPEL 10 Overview
3:29:56 Intermission
3:29:59 KDE Plasma 6
3:58:09 Intermission
3:58:59 Ultramarine Linux
4:16:48 Intermission
4:17:34 Fedora Asahi Linux Remix
4:58:37 Intermission
4:58:39 Closing Remarks
```

If no talk CSV file is provided, sequential names will be generated automatically:

```
0:00:00 Talk #0
0:31:59 Intermission
0:32:14 Talk #1
0:47:47 Intermission
0:47:55 Talk #2
[...]
```
