from ffmpeg import FFmpeg as FFmpegSync, Progress
from pathlib import Path

def combineWebms(inputs, output, ffmpeg_object=None, cleanup=False, yes=False, debug=False):
    # TODO: this needs to be able to respect the intermediates directory. suggest a working directory kwarg
    videoinputsfile = Path("input.txt")
    
    videoinputsfile.write_text('\n'.join([f"file '{i}'" for i in inputs]))

    if ffmpeg_object is None:
        ffmpeg_object = FFmpegSync()
    
    if yes:# this is redundant with the yes in apply common options
        ffmpeg_object.option("y")

    ffmpeg = (
        ffmpeg_object
        .option("threads", value="4")
        .option("f", value="concat")
        .option("safe", value="0")
        .input("input.txt")
        .output(
            output,
            {"c:v": "copy", "c:a": "copy"}
        )
    )
    setup_ffmpeg_output(ffmpeg, debug=debug)
    ffmpeg.execute()
    if cleanup:
        videoinputsfile.unlink()


def talk_title_to_identifier(talkname:str):
    identifier = ''.join(e for e in talkname.strip() if e.isalnum())
    
    return identifier.lower()

def setup_ffmpeg_output(ffmpeg, debug=True):
    # disabling debugging should essentialy disable this
    if not debug:
        return
    # https://python-ffmpeg.readthedocs.io/en/latest/examples/monitoring-status/
    @ffmpeg.on("start")
    def on_start(arguments: list[str]):
        print("arguments:", arguments)

    @ffmpeg.on("stderr")
    def on_stderr(line):
        print("stderr:", line)

    @ffmpeg.on("progress")
    def on_progress(progress: Progress):
        print(progress)

    @ffmpeg.on("completed")
    def on_completed():
        print("completed")

    @ffmpeg.on("terminated")
    def on_terminated():
        print("terminated")


def apply_common_ffmpeg_options(ffmpeg_object, yes=False):
    if yes:
        ffmpeg_object = ffmpeg_object.option("y")
        
    return (ffmpeg_object
        .option("hide_banner")
        .option("loglevel", value="warning")
    )
