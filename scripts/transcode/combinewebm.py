import argparse
from helpers import combineWebms

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Combine several webm files into one.')
    parser.add_argument('inputs', metavar='N', type=str, nargs='+',
                        help='the input files to combine')
    parser.add_argument('output', type=str, help="the output filename for the resulting output")
    parser.add_argument('--cleanup', action='store_true', default=True, help="whether to cleanup intermediate files created")
    parser.add_argument('--yes', action='store_true', default=True, help="whether to pass yes to ffmpeg (essentially allowing things to happen automatically, like overwriting files)")

    args = parser.parse_args()
    
    combineWebms(args.inputs, args.output, cleanup=args.cleanup, yes=args.yes)