import argparse
from pathlib import Path
import csv
import json
import configparser
from wand.image import Image
from ffmpeg import Progress, FFmpeg as FFmpegSync

from helpers import combineWebms, apply_common_ffmpeg_options, setup_ffmpeg_output, talk_title_to_identifier


def createThumbVideo(config, thumbnailImagePath:Path, working_directory:Path, outputPath:str, cleanup=False, yes=False, width=None, height=None, audio_samplerate=None, debug=False):
    thumbnail_path = working_directory / thumbnailImagePath.name
    thumb = Image(filename=thumbnailImagePath)
    target_width = width or config.getint("resolution_width")
    target_height = height or config.getint("resolution_height")
    thumb.resize(target_width,target_height)
    thumb.save(filename=str(thumbnail_path))
    audio_samplerate = audio_samplerate or 44100
    
    # Create still video from the introduction image and silence
    print("creating silence")

    silence_filename = ""
    slide_filename = ""

    silence = (apply_common_ffmpeg_options(FFmpegSync(), yes=yes)
        .option("ar", value=audio_samplerate)
        .option("t", value=config.getint("duration"))
        .input("/dev/zero", {
            "f": "s16le",
            "c:a":"pcm_s16le",
        })   
    )
    if config.get("target_format_mode") == "webm":

        silence_filename = working_directory.joinpath("silence.opus")

        silence = silence.output(silence_filename,
            {
                "ab": "64K",
                "c:a": "libopus"
            }
        )
    elif config.get("target_format_mode") in ["mp4", "mkv"]:
        silence_filename = working_directory.joinpath("silence.aac")
        silence = silence.output(silence_filename,
            {
                "ab": "64K",
                "c:a": "aac"
            }
        )
    
    setup_ffmpeg_output(silence, debug=debug)
    silence.execute()

    print("creating still")
    still = (apply_common_ffmpeg_options(FFmpegSync(), yes=yes)
        .option("loop", value=1)
        .input(thumbnail_path)
    )

    thumb_visual_extension = "." + config.get("target_format_mode")

    slide_filename = working_directory.joinpath(f"thumb_visual{thumb_visual_extension}")

    if config.get("target_format_mode") == "webm":
        still = still.output(slide_filename, {
            "q:v":1,
            "r": config.getint("framerate"),
            "t": config.getint("duration")
        })

    elif config.get("target_format_mode") in ["mp4", "mkv"]:
        still = still.output(slide_filename, {
            "q:v":1,
            "r": config.getint("framerate"),
            "t": config.getint("duration"),
            # "f": config.get("target_format_mode"),
            "vcodec": "libx264"
        })


    setup_ffmpeg_output(still, debug=debug)
    still.execute()

    print("creating final")

    final = (apply_common_ffmpeg_options(FFmpegSync(), yes=yes)
        .input(slide_filename)
        .input(silence_filename)
        .output(outputPath, {
            "c:v": "copy",
            "c:a": "copy",
        },
        map=["0:0", "1:0"],)
    )
    setup_ffmpeg_output(final, debug=debug)
    final.execute()

    if cleanup:
        slide_filename.unlink()
        silence_filename.unlink()



# NOTE(MoralCode) 2024-09-25 this chunk of comments below is a copy paste from the original bash versions of this script
#   this original script supported adding a fade between the titlecard and the content, but it was deemed to not be worth converting
#   to python given the timelines for flock and other early events this was being used for. 
#   it may be useful to re-introduce this functionality later (and make it work with the newer features, like handling of webm, mp4, and mkv formats)
#   therefore it is here as a reference for some probably-mostly-working ffmpeg commands to test against.
# extractVideo () {
#   # Split the portion of the video that we want to work on directly from the source file
#   # converting the video to frame files one PNG file per frame and
#   # splitting the audio to to an mp3 file
#   rm -fr "${INTERMEDIATES_DIR}/${OUTPUTFILE}-frames"
#   mkdir -p "${INTERMEDIATES_DIR}/${OUTPUTFILE}-frames"

#   # split video into frames.
#   # also extract audio into an mkv as needed for the fade
#   ffmpeg ${FFMPEG_COMMON_OPTIONS} -threads ${THREADS} \
#           -i "${INPUTFILE}" -ss ${SCENESTART} -t ${SCENEDURATION} \
#           -f image2 -y "${INTERMEDIATES_DIR}/${OUTPUTFILE}-frames"/frame%d.png

#   ffmpeg ${FFMPEG_COMMON_OPTIONS} -threads ${THREADS} \
#           -ss ${SCENESTART} -i "${INPUTFILE}" -t ${SCENEDURATION} \
#           -c:a libopus -y "${INTERMEDIATES_DIR}/${OUTPUTFILE}-fadeaudio.webm"

#   #NOTE: this assumes that SCENEDURATION is both the duration of the fade and the timestamp
#   # (relative to the original clip) that the scene ends
#   # ffmpeg ${FFMPEG_COMMON_OPTIONS} -threads ${THREADS} \
#   #         -ss ${SCENEDURATION} -i "${INPUTFILE}" \
#   #         -c:a copy -y "${INTERMEDIATES_DIR}/${OUTPUTFILE}-remaining.webm"
# }


# fadeOutIntro () {
#   #$1 seconds
#   SECONDS=$1 
#   FRAMES=$(($FRAMERATE*$SECONDS))
#   echo "Fading out intro over ${SECONDS} seconds (${FRAMES} frames)"
  
#   # Fadeout intro into the first few seconds of the video
#   # because frame numbers are used to look up files here, this is limited to only fading the intro
#   mkdir -p "${INTERMEDIATES_DIR}/${OUTPUTFILE}-frames2"
#   for i in $(seq 1 $FRAMES); do 
#     percent=$(echo "scale=3; ${i}*100/$FRAMES" | bc )
#     magick -compose dissolve -gravity center -define compose:args=${percent} \
#             -composite "${INTERMEDIATES_DIR}/${OUTPUTFILE}-intro.png" "${INTERMEDIATES_DIR}/${OUTPUTFILE}-frames"/frame1.png "${INTERMEDIATES_DIR}/${OUTPUTFILE}-frames2"/frame${i}.png
#   done
# }

# mergeFadedIntroFrames () {
#   # Merge the modified frames onto the original frames
#   cp "${INTERMEDIATES_DIR}/${OUTPUTFILE}-frames2"/frame*.png "${INTERMEDIATES_DIR}/${OUTPUTFILE}-frames"

#    # Reassembles the modified frames and the audio into an output video
#   ffmpeg ${FFMPEG_COMMON_OPTIONS} -threads ${THREADS} \
#             -r ${FRAMERATE} -f image2 -i "${INTERMEDIATES_DIR}/${OUTPUTFILE}-frames"/frame%d.png \
#             -map 0:0  -y "${INTERMEDIATES_DIR}/${OUTPUTFILE}-vidfaded.webm"
#             # -i "${INTERMEDIATES_DIR}/${OUTPUTFILE}-fadeaudio.webm" -c:a libopus \-map 1:0
# }



# no - overlayImage () {
#   # Overlays an image on top of each frame in a range.
#   FILE="${1}"
#   GRAVITY=$2
#   START_FRAME=$(echo "scale=0; ${3}*29.97/1" | bc )
#   END_FRAME=$(echo "scale=0; ${4}*29.97/1" | bc )

#   for (( i=${START_FRAME} ; i<=${END_FRAME} ; i++ )) ; do 
#     magick -compose dissolve -gravity ${GRAVITY} -define compose:args=90 \
#             -composite "${OUTPUTFILE}-frames"/frame${i}.png "${FILE}" "${OUTPUTFILE}-frames2"/frame${i}.png
#   done
# }

def ensure_directory(config, section, name):
    workdir = Path(config.get(section, name))
    if not workdir.exists():
        workdir.mkdir()
    return workdir

def ensure_working_directory(config, section):
    return ensure_directory(config, section, "working_directory")


def probe(inputfile:str):
    # https://python-ffmpeg.readthedocs.io/en/latest/examples/querying-metadata/
    ffprobe = FFmpegSync(executable="ffprobe").input(
        inputfile,
        print_format="json", # ffprobe will output the results in JSON format
        show_streams=None,
    )

    media = json.loads(ffprobe.execute())
    return media


def editSingleVideo(config, contentFile, videoData, title, outDir, cleanup=False, yes=False, debug=False):

    output_file_title = title

    output_file_title = output_file_title.replace("/", "") # this causes problems when writing files
    output_file_title = output_file_title.replace("'", "") # this causes problems when reconcatenating the video

    output_ext = "." + config['DEFAULT'].get("target_format_mode")

    currentConfigSection = 'input'

    input_content_location = ensure_directory(config, currentConfigSection, "content_directory")

    media = probe(Path(input_content_location, contentFile))

    # # only needed if doing a fade.
    print(f"Beginning initial setup...")
    workdir = ensure_working_directory(config, currentConfigSection)

    # # TODO: confirm all the needed locations (mostly inputfile and thumbnail) are accessible and cleanly error if they are not

    print(f"Processing {contentFile}...")
    # extractVideo &  

    currentConfigSection = 'titlecard'

    contentpath = ensure_directory(config, currentConfigSection, "content_directory")

    titlecardDuration = config.getint(currentConfigSection, "duration")
    print(f"Beginning creation of thumb clip ({titlecardDuration} seconds)...")
    workdir = ensure_working_directory(config, currentConfigSection)
    thumbOutputPath = workdir.joinpath(f"{output_file_title}-intro{output_ext}")
    createThumbVideo(
        config[currentConfigSection],
        contentpath.joinpath(videoData["thumbnail_name"]),
        workdir,
        str(thumbOutputPath),
        cleanup=cleanup,
        yes=yes,
        width=int(media['streams'][0]['width']),
        height=int(media['streams'][0]['height']),
        debug=debug
    )

    # NOTE(MoralCode): 2024-09-24 This currently isnt used for fedora events
    #   but might be cool to use in the future so the end card can be different from the title card
    #   As a result, this wasnt tested and will likely also require small tweaks to the combineWebms step below to get working
    # if config.has_section("endcard"):
    #     # print(f"CREATE EXIT ROUTINE")
    #     currentConfigSection = 'endcard'
    #     workdir = ensure_working_directory(config, currentConfigSection)
    #     endcardOutputPath = workdir.joinpath(f"{output_file_title}-exit.webm")
    #     createThumbVideo(config[currentConfigSection], Path(videoData["endcard_path"]), workdir, str(endcardOutputPath), cleanup=cleanup, yes=yes)

    print(f"processing")
    # wait

    if config.has_section("titlecard.fade"):
        # print(f"Creating overlays")
        currentConfigSection = 'titlecard.fade'
        workdir = ensure_working_directory(config, currentConfigSection)

        # # http://askubuntu.com/questions/407743/ddg#407761
        # fadeOutIntro $( echo $SCENEDURATION | awk -F: '{ print ($1 * 3600) + ($2 * 60) + $3 }' )
        # mergeFadedIntroFrames

    print(f"reassembling video")
    combineWebms([
        thumbOutputPath,
        input_content_location.joinpath(contentFile),
        thumbOutputPath,
    ], outDir.joinpath(f"{output_file_title}{output_ext}"),
    yes=yes,
    debug=debug)

    print(f"Done!")


# https://python-ffmpeg.readthedocs.io/en/latest/
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='generate a script to edit the videos from a CSV')
    parser.add_argument('csvfile', help='the csv file containing the talks to process')
    parser.add_argument('--config', default="config.ini", help='the path to the config file defining how each video should be edited')
    parser.add_argument('--debug', default=False, action="store_true", help='include additional information to troubleshoot the program')

    args = parser.parse_args()

    csvpath = Path(args.csvfile)

    if not Path(args.config).exists():
        raise ValueError(f"The provided config file {args.config} does not exist")

    config = configparser.ConfigParser()
    config.read(args.config)

    
    currentConfigSection = 'output'

    # create out dir
    outdir = ensure_working_directory(config, currentConfigSection)

    prefix = config[currentConfigSection].get("filename_prefix", "")

    with csvpath.open() as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            filename = row.get('Identifier', "").strip()
            if filename == "":
                filename = talk_title_to_identifier(row.get('Title'))
            filename_uscore = filename.replace("-", "_")
            video_ext = "." + config['DEFAULT'].get("target_format_mode")
            thumb_ext = ".png"
            editSingleVideo(
                config,
                row.get('Filename', f"{filename[:100]}{video_ext}"),
                {"thumbnail_name": row.get('Thumbnail', f"{filename_uscore}{thumb_ext}")},
                f"{prefix} {row.get('Title')}",
                outdir, 
                yes=True,
                debug=args.debug
            )

    
