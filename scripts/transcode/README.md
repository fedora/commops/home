# TRANSCODE

The Tailored Resilient Automation Netting Speedy Commmunity Observation of Developments Externally

This is a script that is designed to help automatically edit title and end cards onto video clips from livestreams as part of the [Livestream processing SOP](https://docs.fedoraproject.org/en-US/marketing/sops/stream-uploads/).

It is primarily designed to accept video clips (cut apart during previous steps of the process) and thumbnail images (created as part of the SOP) as input. 


This program currently provides the following features/benefits:
- Automatically convert thumbnails into video clips of custom duration and prepend/append them to the video contents



## Story behind the name
I picked it as a low-effort name
then retroactively made up an acronym because I didnt feel like changing the name of the folder

In actuality, this script tries to *avoid* transcoding anything wherever possible as that takes a lot of time, processing power, and installed codecs that may or may not be set up (especially if you run stock Fedora).

## Usage

### You will need
1. **Dependencies**. Install the dependencies by running `pip install -r requirements.txt` in a relatively modern python environment, such as python 3.12.
2. **CSV Talk Metadata**. Many of the scripts in this workflow use a CSV file (containing minimally `Title` and `Identifier` columns) to retrieve metadata about the talks being processed. This script mainly uses these to automate the creation of filenames so it knows where to generate files that it will need later.
3. **Edited Videos**. By the time you use this script, you should already have the videos cut up and saved in a folder, named according to their `Identifier` value in the CSV (plus whatever extension they were saved with, such as `.webm`, `.mp4`, or `.mkv`)
4. **Generated Thumbnails**. By the time you use this script, you should already have the thumbnails generated and saved in a folder (ideally separate from the videos for your own sanity). These should also be named according to their `Identifier` value in the CSV.


### Configuring
This program receives configuration primarily in two ways:

1. a CSV file containing the files to be edited
2. a .ini file containing configuration that applies to every video being processed, such as dimensions, working directories, output directories, etc.

You can find a default config at [config.ini](./config.ini). This config file has many top level defaults and is also solit into sections based on the stage of the process. While some config options are only meaningful to specific parts of the process, many are shared across all of them. It is recommended to read through all the comments in this file to ensure each value is set correctly (some are not yet fully automated).

The script also accepts some basic arguments to allow you to specify where to find these CSV and INI files. Run `python3 ./videoedit.py --help` for usage information about these arguments. 

### An Ideal Video Processing Procedure

Here is an example of the workflow and usage that this script was designed for:

1. Ensure your directories are set up. Some of the earlier scripts in this process will automatically create folders for you to help organize things (such as one folder per room from a conference). Ensure you are inside the folder for the set of videos you want to process.
2. ensure you have the dependencies installed `pip install -r requirements.txt`
3. Set up the config.ini for your particular event. Notably here are some values you really should at least take a look at:
     - `target_format_mode`. This needs to be set correctly. `mkv` is the recommended value if your edited videos are in `mp4` or `mkv` format. Use `webm` if your edited videos are in `webm` format
     - `content_directory`. If you are storing different files (thumbnails, video clips .etc) in different folders, ensure this is set to the correct value within each stage
     - `duration` ( under `[titlecard]`). ensure this is the duration you want the thumbnail to display for
     - `framerate` ( under `[titlecard]`). ensure this is set to the framerate of your video clips. It is not currently automatically detected
     - `filename_prefix`. remove the `;` before this line and change the value if you want all output videos to have a prefix on their name, such as to denote videos from your particular conference (this helps video platforms select a good title upon upload)  
4. If your video clips are in `.mp4` format (in shich case the `target_format_mode` should be `mkv`), youll need to run a quick conversion script to create mkv copies of your videos. From within the folder where your videos are located, run the `tomkvbulk.sh` script thats included in this folder. This should be a very quick (under 30 seconds) process and should ensure tat the videos are available in the correct place to be found by the script
5. From here, you can run the script, which should look something like `python3 ../path/to/videoedit.py path/to/talks.csv`. If your `.ini` file is named something other than the default (`config.ini`), you can specify where to find it using `--config`, such as: `python3 ../path/to/videoedit.py path/to/talks.csv --config path/to/somethingelse.ini`


When each video is done processing, the script should print `Done!` on its own line.

Here is an example of what a successful run looks like:

```
Beginning initial setup...
Processing flock-2024-101-marketing-team-hopes-dreams-updates-.mkv...
Beginning creation of thumb clip (5 seconds)...
creating silence
creating still
creating final
processing
reassembling video
Done!
```


If you would like additional output to help debug an issue, add the `--debug` argument to the script. This will enable the display of the output from `ffmpeg` that is generated by each of these steps.