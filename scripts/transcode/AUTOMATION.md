# Automation

I think it would be nice to eventually get this pipeline near-fully automated, but that seems like more effort than it may be worth so i'll just document my thoughts here for how I was thinking of approaching it

**Infrastructure and Preliminary Research**
- [ ] Look into how to do authentication with youtube (https://github.com/ThioJoe/YT-Spammer-Purge interacts with the youtube API for helping mass-report comment spam, and has a rather complicated process involving creating a thing in a google cloud portal)
- [ ]  Research how to detect when a youtube video livestream ends and becomes a VOD 
- [ ]  Set up or add a plugin to integrate with a matrix bot (such as [eventbot](https://github.com/fedora-infra/maubot-pretix-invite)) so certain channels or people can be pinged as part of the pipeline


**The Process**

many of these steps have some basis in existing work but this is written idealistically as if everything worked the way i wanted it to, so some assumptions made here may take a while to fulfill

1. event livestream ends
2. bot (which would already be connected to the fedora youtube) detects that a livestream ended and became a VOD
3. ping a human for input. Human needs to:
 - follow the instructions in https://docs.fedoraproject.org/en-US/marketing/sops/stream-uploads/ to tag the intermissions between each speaker in the livestream
 - upload the CSV file (from that SOP, likely from the marketing team) containing the list of speaker segments, talk titles, etc for that days livestream only.
 - upload a template SVG for the thumbnail and/or end card containing backgrounds, icons, and placeholder text so that the images can be generated
 - config options (like whether to mark the original livestream as unlisted, reupload schedule)
4. wait for sponsorblock API to return the right number of timestamp segments for the livestream clip
5. use the timestamp script (https://gitlab.com/fedora/commops/home/-/merge_requests/4) to generate the timestamps and chapter markers for the youtube livestream
6. edit the description of the video using the youtube API to append this set of timestamps (so youtube can do chapters)
7. generate the thumbnails (based on something like [this process](https://docs.fedoraproject.org/en-US/marketing/sops/stream-uploads/#_3_creating_the_thumbnails)).
8. [use `yt-dlp` to download the livestream chapters and automatically split them apart](https://docs.fedoraproject.org/en-US/marketing/sops/stream-uploads/#_2_1_using_yt_dlp)
9. use the script in this folder to turn the tumbnail and endcard images into videos and combine each talk with its respective thumbnails. (some editing may be needed to have it read from the same CSV file provided by the user for convenience)
10. maybe, at the users option (see provided config options), set the original livestream to unlisted using the youtube API access
11. ping a human to review each video before publishing to make sure it is okay to publish and didnt get horribly messed up (no typos in speaker name or talk title, .etc)
12. if approved, upload and/or schedule the videos to be re-released per the users options. Possibly add the videos to a new playlist for the event (or reuse a playlist if this is a "day 2" livestream).
13. [Optional] create/export some kind of report to save all the inputs and a list of what was approved when and by who (may be nice as like an email report to alert someone that it succeesed if this is run as a kind of CI pipeline)



## Future Feature Ideas (probably for another script)
this was saved from the readme during a major edit

Future feature ideas:
- ensure captions are preserved and/or generated and included in (or generated alongside) clips so they are present when uploaded to youtube
- automatically edit the youtube livestream to add chapter timestamps once the breaks are tagged in sponsorblock  
- a "batch run" mode using the CSV file from timestamp generation
- automatically detect the completion of a livestream (or chapters being added to an existing VOD) and allow for an automatic/CI mode to begin the editing process completely hands-free
- automatically perform the re-upload of clips to youtube when complete
- automatically mark the original livestream as unlisted
- bulk-set the descriptions and other metadata of the re-uploaded videos to specify what event they came from, the topic, and the speakers.