<!-- This template is for Fedora Community Ops team members to nominate a new member to join the team.-->

I am opening a nomination ticket for **NAME** to join the Fedora Community Ops Team.

* **Name**:
* **FAS username**:
* **Reason for nomination**:
* **Link(s) to contribution examples**:
    * https://
    * https://
    * https://

I am seeking three additional `+1` votes from other Fedora Community Ops Team members to confirm this sponsorship.

<!--DO NOT EDIT BELOW THIS LINE!-->

/labels ~"category::onboarding" ~"scope::new" ~"?::needs team vote"

