How to contribute to CommOps documentation
==========================================

<!--
    Style rule: one sentence per line please!
    This makes git diffs easier to read. :)
-->

Please report issues and submit pull requests for **Content Fixes** to [Fedora CommOps documentation](https://docs.fedoraproject.org/en-US/commops/) here.
General appearance issues and publishing issues should be reported against the [publishing software](https://pagure.io/fedora-docs/docs-fp-o).

Never done a pull request (or "PR")?
See the [GitHub Guides](https://guides.github.com/) for getting started with git.
If you already know git, see the [Pagure documentation for Pull Requests](https://docs.pagure.org/pagure/usage/pull_requests.html) on how to submit contributions on pagure.io.


## How to edit these documents

Content for the CommOps documentation is stored [in this repository](https://pagure.io/fedora-commops/tree/).
Source content is written in [AsciiDoc](http://asciidoc.org/).
See these resources for help using AsciiDoc:

* [AsciiDoc Syntax - Quick Reference](http://asciidoctor.org/docs/asciidoc-syntax-quick-reference/)
* [AsciiDoc Recommended Practices](https://asciidoctor.org/docs/asciidoc-recommended-practices/)

<!--

Advanced topics for AsciiDoc:

* [AsciiDoc Writer's  Guide](http://asciidoctor.org/docs/asciidoc-writers-guide/)
* [Antora Documentation](https://docs.antora.org/antora/1.0/page/)

-->


## Test with a local preview

This repo includes scripts to build and preview the contents of this repository.

**NOTE**: Please note that if you reference pages from other repositories, those links will be broken in the local preview, as it only builds this repository.
If you want to rebuild the whole Fedora Docs site, see the [Fedora Docs build repository](https://pagure.io/fedora-docs/docs-fp-o/) for instructions.

Both of the below scripts use Docker, so ensure Docker is installed on your system before beginning (see below for Fedora instructions).
See below for further instructions.

To build and preview the site, run:

```bash
./build.sh && ./preview.sh
```

The preview is hosted at [http://localhost:8080](http://localhost:8080).

### Installing Docker on Fedora

See the Fedora Developer Portal on [how to install Docker on Fedora](https://developer.fedoraproject.org/tools/docker/docker-installation.html "How to install Docker on Fedora")
