Community + Operations = **CommOps**
====================================

<!--
    Style rule: one sentence per line please!
    This makes git diffs easier to read. :)
-->

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/ "Fedora Docs content licensed under CC BY-SA 4.0")
[![Join us at #commops on Matrix](https://img.shields.io/badge/Chat-in?style=flat&logo=Matrix&label=%23commops%3Afedoraproject.org&link=https%3A%2F%2Fmatrix.to%2F%23%2F%23commops%3Afedoraproject.org)](https://matrix.to/#/#commops:fedoraproject.org "Join us at #commops on Matrix")

The Fedora Community Operations Team provides tools, resources, and utilities for different sub-projects of Fedora to improve effective communication.

## What CommOps does

The following areas are examples of where CommOps focuses:

* Work closely with [Fedora Community Architect](https://docs.fedoraproject.org/en-US/council/fca/) to prioritize key focus areas
* Assist [Fedora Operations Architect](https://docs.fedoraproject.org/en-US/council/foa/) with release preparations
* Support preparation and execution of [Fedora Elections](https://fedoraproject.org/wiki/Elections)
* Work with sub-projects and teams to improve on-boarding methods and practices
* Use metrics and data to improve understanding of Fedora community
    * Support development of metrics tooling

Read more about us on [docs.fedoraproject.org](https://docs.fedoraproject.org/en-US/commops/ "Fedora Community Operations [CommOps] :: Fedora Docs").


## What can I do here?

[File a ticket](https://gitlab.com/fedora/commops/home/-/issues/new) for topics such as the following:

* Culture
* Elections
* Storytelling
* Metrics
* Supporting sub-projects

You can also help us!
Read our [getting started guide](https://docs.fedoraproject.org/en-US/commops/join/) for new contributors to get involved.
After reviewing the guide, check out any open tickets marked as [**good first issue**](https://gitlab.com/fedora/commops/home/-/issues/?sort=created_date&state=opened&label_name%5B%5D=good%20first%20issue&first_page_size=100).
If you are not sure where to start, ask any questions or doubts on the [Discussion forum](https://discussion.fedoraproject.org/tags/c/project/7/commops-team).

If an issue needs urgent attention, contact the ticket assignee to check on their progress. If you don’t hear back within a week, you can take ownership of the task.

As our team is mostly volunteers, we may not notice new issues immediately. Thank you for your understanding!

## Get in touch with us

The Fedora Community Operations (CommOps) Team uses both a [Fedora Discussion forum](https://discussion.fedoraproject.org/tags/c/project/7/commops-team) tag and a [Fedora Chat/Matrix](https://matrix.to/#/#commops:fedoraproject.org) room for communication.

- **Fedora Discussion Forum:** This forum is ideal for asynchronous communication, where responses may come later. It’s the best place for longer, threaded discussions or when you have a question that doesn’t need an immediate answer.
- **Fedora Chat/Matrix Room:** This chat is optimal for synchronous, real-time conversations. Use it when you need quick feedback or want to discuss something actively and interactively with other team members.

## Get involved

Read our guide on [**how to join the Fedora Community Operations Team**](https://docs.fedoraproject.org/en-US/commops/join/)

The team is always excited to welcome new contributors. Whether you’re just starting out or a seasoned Fedora member, there are plenty of ways to get involved and make an impact.
